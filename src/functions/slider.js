import { Movie } from "../models/movie";

export function getMovies(cb, film) {
  var xhr = new XMLHttpRequest();
  xhr.open(
    "GET",
    "https://fake-movie-database-api.herokuapp.com/api?s=" + film
  );

  xhr.addEventListener("loadend", function () {
    var data = JSON.parse(xhr.response);

    var movies = data.Search.map(function (m) {
      return new Movie(m.imdbID, m.Title, m.Poster);
    });

    cb(movies);
  });

  xhr.send();
}

export function sliceMovies(movies, from, to) {
  var idx = from;
  var goOn = true;
  var ret = [];

  do {
    if (idx == to) {
      goOn = false;
    }

    ret.push(movies[idx]);
    idx++;

    if (idx === movies.length) {
      idx = 0;
    }
  } while (goOn);

  return ret;
}
