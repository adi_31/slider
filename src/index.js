// entry point
import "bootstrap/dist/js/bootstrap";
import { getMovies, sliceMovies } from "./functions/slider";
import { getUsers } from "./functions/users";

var tbody = document.getElementById("tbody");

var from = 0;
var to = 3;

var movies = [];

document
  .querySelector('select[name="film"]')
  .addEventListener("change", function (e) {
    var film = e.target.value;
    slider.innerHTML = "";

    from = 0;
    to = 3;
    movies = [];

    getMovies(
      function (apiMovies) {
        movies = apiMovies;
        sliceMovies(movies, from, to).forEach(function (movie) {
          slider.innerHTML +=
            '<div class="col-lg-3"><img class="poster" src="' +
            movie.image +
            '"></div>';
        });
      },
      film === "0" ? "batman" : film
    );
  });

getUsers(function (users) {
  users.forEach(function (user) {
    tbody.innerHTML =
      "<tr><td>" +
      user.id +
      "</td>" +
      user.email +
      "</td><td>" +
      user.username +
      "</td></tr>";
  });
});

var slider = document.getElementById("slider");

function handleSliderChange(isNext) {
  if (isNext) {
    from = from === movies.length - 1 ? 0 : ++from;
    to = to === movies.length - 1 ? 0 : ++to;
  } else {
    from = from === 0 ? movies.length - 1 : --from;
    to = to === 0 ? movies.length - 1 : --to;
  }

  // slider.innerHTML = "";
  // sliceMovies(movies, from, to).forEach(function (movie) {
  //   slider.innerHTML +=
  //     '<div class="col-lg-3"><img class="poster" src="' +
  //     movie.image +
  //     '"></div>';
  // });
}

document.querySelectorAll("button.slider-action").forEach(function (b) {
  b.addEventListener("click", function (e) {
    handleSliderChange(e.target.id === "slider-next");
  });
});

const slices = sliceMovies(movies, from, to);

for (const slice of slices) {
  slider.innerHTML +=
  '<div class="col-lg-3"><img class="poster" src="' +
  movie.image +
  '"></div>';
}